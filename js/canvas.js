var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
//primer cuadrado de la casa
ctx.beginPath();
ctx.strokeStyle = "yellow";
ctx.moveTo(30,120);
ctx.lineTo(110,120);

ctx.moveTo(110,120);
ctx.lineTo(110,60);

ctx.moveTo(30,120);
ctx.lineTo(30,60);
ctx.stroke();
//diagonales de la casa
ctx.beginPath();
ctx.strokeStyle = "brown";
ctx.moveTo(24,60);
ctx.lineTo(70,20);

ctx.moveTo(116,60);
ctx.lineTo(70,20);

ctx.moveTo(36,60);
ctx.lineTo(70,30);

ctx.moveTo(104,60);
ctx.lineTo(70,30);
ctx.stroke();
//cerrar diagonales
ctx.beginPath();
ctx.strokeStyle = "brown";
ctx.moveTo(24,60);
ctx.lineTo(36,60);

ctx.moveTo(116,60);
ctx.lineTo(104,60);
ctx.stroke();
//primera ventana
ctx.beginPath();
ctx.strokeStyle = "violet";
ctx.moveTo(40,62);
ctx.lineTo(63,62);

ctx.moveTo(40,62);
ctx.lineTo(40,77);

ctx.moveTo(40,77);
ctx.lineTo(63,77);

ctx.moveTo(63,62);
ctx.lineTo(63,77);

ctx.moveTo(40,67);
ctx.lineTo(63,67);

//segunda ventana 
ctx.moveTo(100,62);
ctx.lineTo(77,62);

ctx.moveTo(100,62);
ctx.lineTo(100,77);

ctx.moveTo(100,77);
ctx.lineTo(77,77);

ctx.moveTo(77,77);
ctx.lineTo(77,62);

ctx.moveTo(100,67);
ctx.lineTo(77,67);

//tercera ventana
ctx.moveTo(40,82);
ctx.lineTo(63,82);

ctx.moveTo(40,82);
ctx.lineTo(40,97);

ctx.moveTo(40,97);
ctx.lineTo(63,97);

ctx.moveTo(63,82);
ctx.lineTo(63,97);

ctx.moveTo(40,87);
ctx.lineTo(63,87);

//segunda ventana 
ctx.moveTo(100,82);
ctx.lineTo(77,82);

ctx.moveTo(100,82);
ctx.lineTo(100,97);

ctx.moveTo(100,97);
ctx.lineTo(77,97);

ctx.moveTo(77,97);
ctx.lineTo(77,82);

ctx.moveTo(100,87);
ctx.lineTo(77,87);

ctx.stroke();
//segundo cuadrado de la casa 
ctx.beginPath();
ctx.strokeStyle = "blue";
ctx.moveTo(110,115);
ctx.lineTo(270,115);

ctx.moveTo(270,115);
ctx.lineTo(270,55);
ctx.stroke();
ctx.beginPath();
ctx.strokeStyle = "brown";
ctx.moveTo(270,55);
ctx.lineTo(110,55);

//techo

ctx.moveTo(270,55);
ctx.lineTo(285,55);

ctx.moveTo(285,55);
ctx.lineTo(270,30);

ctx.moveTo(270,30);
ctx.lineTo(82,30);
ctx.stroke();
//escaleras
ctx.beginPath();
ctx.strokeStyle = "silver";
ctx.moveTo(150,115);
ctx.lineTo(220,115);

ctx.moveTo(150,120);
ctx.lineTo(220,120);

ctx.moveTo(150,115);
ctx.lineTo(150,120);

ctx.moveTo(220,115);
ctx.lineTo(220,120);

ctx.moveTo(220,120);
ctx.lineTo(225,125);

ctx.moveTo(150,120);
ctx.lineTo(145,125);

ctx.moveTo(225,125);
ctx.lineTo(145,125);
ctx.stroke();
//puerta
ctx.beginPath();
ctx.strokeStyle = "purple";
ctx.moveTo(170,115);
ctx.lineTo(170,90);

ctx.moveTo(200,115);
ctx.lineTo(200,90);

ctx.moveTo(200,90);
ctx.lineTo(170,90);
ctx.stroke();
//adorno puerta
ctx.beginPath();
ctx.strokeStyle = "green";
ctx.moveTo(175,110);
ctx.lineTo(195,110);

ctx.moveTo(175,103);
ctx.lineTo(195,103);

ctx.moveTo(175,110);
ctx.lineTo(175,103);

ctx.moveTo(195,110);
ctx.lineTo(195,103);

ctx.moveTo(175,100);
ctx.lineTo(195,100);

ctx.moveTo(175,95);
ctx.lineTo(195,95);

ctx.moveTo(175,100);
ctx.lineTo(175,95);

ctx.moveTo(195,100);
ctx.lineTo(195,95);
ctx.stroke();
//adorno recta al lado del circular
ctx.moveTo(150,115);
ctx.lineTo(150,90);
ctx.moveTo(155,115);
ctx.lineTo(155,90);

ctx.moveTo(220,115);
ctx.lineTo(220,90);
ctx.moveTo(215,115);
ctx.lineTo(215,90);

ctx.moveTo(222,90);
ctx.lineTo(213,90);
ctx.moveTo(148,90);
ctx.lineTo(157,90);

ctx.arc(185,128,49, (Math.PI/180)*230, (Math.PI/180)*308,false);
ctx.moveTo(148,90);
ctx.arc(185,122,50, (Math.PI/180)*220, (Math.PI/180)*320,false);
ctx.stroke();
//VENTANAS INTERNAS
//lado izq
ctx.beginPath();
ctx.strokeStyle = "red";
ctx.moveTo(117,65);
ctx.lineTo(140,65);

ctx.moveTo(117,65);
ctx.lineTo(117,95);

ctx.moveTo(117,95);
ctx.lineTo(140,95);

ctx.moveTo(140,95);
ctx.lineTo(140,65);

ctx.moveTo(117,75);
ctx.lineTo(140,75);

ctx.moveTo(117,80);
ctx.lineTo(140,80);
ctx.moveTo(117,85);
ctx.lineTo(140,85);
ctx.moveTo(117,90);
ctx.lineTo(140,90);

ctx.moveTo(128.5,75);
ctx.lineTo(128.5,95);

//lado der

ctx.moveTo(230,65);
ctx.lineTo(253,65);

ctx.moveTo(230,65);
ctx.lineTo(230,95);

ctx.moveTo(230,95);
ctx.lineTo(253,95);

ctx.moveTo(253,95);
ctx.lineTo(253,65);

ctx.moveTo(230,75);
ctx.lineTo(253,75);

ctx.moveTo(230,80);
ctx.lineTo(253,80);
ctx.moveTo(230,85);
ctx.lineTo(253,85);
ctx.moveTo(230,90);
ctx.lineTo(253,90);

ctx.moveTo(241.5,75);
ctx.lineTo(241.5,95);
ctx.stroke();
//chimenea 
ctx.beginPath();
ctx.strokeStyle = "pink";
ctx.moveTo(260,30);
ctx.lineTo(260,15);
ctx.moveTo(230,30);
ctx.lineTo(230,15);
ctx.moveTo(265,15);
ctx.lineTo(225,15);

ctx.moveTo(265,15);
ctx.lineTo(265,10);

ctx.moveTo(225,15);
ctx.lineTo(225,10);

ctx.moveTo(225,10);
ctx.lineTo(265,10);
ctx.stroke();


//personas
ctx.strokeStyle = "#1E90FF";
ctx.moveTo(238,145);
ctx.lineTo(258,145);
ctx.moveTo(238,145);
ctx.lineTo(238,135);
ctx.moveTo(258,145);
ctx.lineTo(258,135);
ctx.moveTo(258,135);
ctx.lineTo(238,135);
ctx.moveTo(253,126.5);
ctx.arc(248, 126.5, 6, 0, 2 * Math.PI);

ctx.moveTo(268,145);
ctx.lineTo(283,145);
ctx.moveTo(268,145);
ctx.lineTo(268,135);
ctx.moveTo(283,145);
ctx.lineTo(283,135);
ctx.moveTo(268,135);
ctx.lineTo(283,135);
ctx.moveTo(279,128.5);
ctx.arc(275.5, 128.5, 4, 0, 2 * Math.PI);
ctx.fill();

ctx.moveTo(293,145);
ctx.lineTo(298,145);
ctx.moveTo(293,145);
ctx.lineTo(293,135);
ctx.moveTo(298,145);
ctx.lineTo(298,135);
ctx.moveTo(293,135);
ctx.lineTo(298,135);
ctx.moveTo(298,130);
ctx.arc(295.5, 130, 3, 0, 2 * Math.PI);

ctx.fillStyle = 'orange';
ctx.fill();
ctx.stroke();



